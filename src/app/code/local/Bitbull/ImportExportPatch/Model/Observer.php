<?php
/**
 * Class     Observer.php
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_ImportExportPatch_Model_Observer
{
    public function checkDependencyToRewrite($observer){


        if($this->isAvsFastSimpleImportInstaller()){

            //check if
            if($this->_checkAvsRewriteImport()){
                //AVS new version
                $config = Mage::getConfig();
                $config->setNode(
                    'global/models/fastsimpleimport/rewrite/import_entity_product',
                    'Bitbull_ImportExportPatch_Model_Import_Entity_ProductAvs'
                );
            }
        }

    }

    private function isAvsFastSimpleImportInstaller()
    {
       return Mage::helper('core')->isModuleEnabled('AvS_FastSimpleImport');
    }

    private function _checkAvsRewriteImport()
    {
        $config = Mage_ImportExport_Model_Config::getModels('global/importexport/import_entities');
        return $config['catalog_product']['model']=='fastsimpleimport/import_entity_product';
    }
} 