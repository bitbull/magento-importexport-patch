<?php
/**
 * Class     Product.php
 * @author   Mirko Cesaro <mirko.cesaro@bitbull.it>
 */

class Bitbull_ImportExportPatch_Model_Import_Entity_Product extends Mage_ImportExport_Model_Import_Entity_Product{

    const DEFAULT_PRESERVE_LINKS = true;

    public function __construct()
    {
        parent::__construct();
        if(Mage::helper('core')->isModuleEnabled('Bitbull_ImportExport')){
            $this->_imagesArrayKeys = Mage::helper('bitbull_importexport')->getDynamicImageArrayKeys();
        }

    }

    public function setPreserveLink( $value){
        $this->_parameters['preserve_links'] = $value;
        return $this;
    }

    public function getPreserveLink()
    {
        if (!isset($this->_parameters['preserve_links'])){
            return self::DEFAULT_PRESERVE_LINKS;
        }
        return $this->_parameters['preserve_links'];
    }

    /**
     * Added dirty hack: extra method because module Avs_FastSimpleImport breaks native importer with the following error
     *
     * <b>Fatal error</b>: Call to undefined method Mage_ImportExport_Model_Import_Entity_Product::filterRowData() in <b>/var/www/magento/app/code/community/AvS/FastSimpleImport/Model/Import/Entity/Product/Type/Configurable.php</b> on line <b>166</b><br />
     *
     * @return $this
     */
    public function filterRowData()
    {
        return $this;
    }

    /**
     * Rewrited this to not replace links when they are not in the import
     * @return $this|Mage_ImportExport_Model_Import_Entity_Product
     */
    protected function _saveLinks()
    {
        $resource       = Mage::getResourceModel('catalog/product_link');
        $mainTable      = $resource->getMainTable();
        $positionAttrId = array();
        /** @var Varien_Db_Adapter_Interface $adapter */
        $adapter = $this->_connection;

        // pre-load 'position' attributes ID for each link type once
        foreach ($this->_linkNameToId as $linkName => $linkId) {
            $select = $adapter->select()
                ->from(
                    $resource->getTable('catalog/product_link_attribute'),
                    array('id' => 'product_link_attribute_id')
                )
                ->where('link_type_id = :link_id AND product_link_attribute_code = :position');
            $bind = array(
                ':link_id' => $linkId,
                ':position' => 'position'
            );
            $positionAttrId[$linkId] = $adapter->fetchOne($select, $bind);
        }
        $nextLinkId = Mage::getResourceHelper('importexport')->getNextAutoincrement($mainTable);
        while ($bunch = $this->_dataSourceModel->getNextBunch()) {
            $productIds   = array();
            $linkRows     = array();
            $positionRows = array();

            foreach ($bunch as $rowNum => $rowData) {
                $this->_filterRowData($rowData);
                if (!$this->isRowAllowedToImport($rowData, $rowNum)) {
                    continue;
                }
                if (self::SCOPE_DEFAULT == $this->getRowScope($rowData)) {
                    $sku = $rowData[self::COL_SKU];
                }
                foreach ($this->_linkNameToId as $linkName => $linkId) {
                    $productId    = $this->_newSku[$sku]['entity_id'];
                    $productIds[] = $productId;
                    if (isset($rowData[$linkName . 'sku'])) {
                        $linkedSku = $rowData[$linkName . 'sku'];

                        if ((isset($this->_newSku[$linkedSku]) || isset($this->_oldSku[$linkedSku]))
                            && $linkedSku != $sku) {
                            if (isset($this->_newSku[$linkedSku])) {
                                $linkedId = $this->_newSku[$linkedSku]['entity_id'];
                            } else {
                                $linkedId = $this->_oldSku[$linkedSku]['entity_id'];
                            }
                            $linkKey = "{$productId}-{$linkedId}-{$linkId}";

                            if (!isset($linkRows[$linkKey])) {
                                $linkRows[$linkKey] = array(
                                    'link_id'           => $nextLinkId,
                                    'product_id'        => $productId,
                                    'linked_product_id' => $linkedId,
                                    'link_type_id'      => $linkId
                                );
                                if (!empty($rowData[$linkName . 'position'])) {
                                    $positionRows[] = array(
                                        'link_id'                   => $nextLinkId,
                                        'product_link_attribute_id' => $positionAttrId[$linkId],
                                        'value'                     => $rowData[$linkName . 'position']
                                    );
                                }
                                $nextLinkId++;
                            }
                        }
                    }
                }
            }

            /* CASI D'USO:
            - cancelliamo i link se viene impostato a false la variabile per preservare i link
                (rispettando la condizione che il comportamento è diverso da append)
            - se sono presenti dei nuovi link da aggiungere e il comportamento è diverso da append
                anche se preserveLinks è true varranno cancellati i link (questo perché è stata passata
                la colonna dei link e quindi il comportamento replace deve sostituirli) ATTENZIONE che se
                il batch da importare non ha valori nei link questi non vengono cancellati anche se
                preserveLinks è false)
            */
            if (
                ($linkRows || !$this->getPreserveLink() ) &&
                Mage_ImportExport_Model_Import::BEHAVIOR_APPEND != $this->getBehavior() &&
                $productIds
            ) {
                $adapter->delete(
                    $mainTable,
                    $adapter->quoteInto('product_id IN (?)', array_unique($productIds))
                );
            }
            if ($linkRows) {
                $adapter->insertOnDuplicate(
                    $mainTable,
                    $linkRows,
                    array('link_id')
                );
                $adapter->changeTableAutoIncrement($mainTable, $nextLinkId);
            }
            if ($positionRows) { // process linked product positions
                $adapter->insertOnDuplicate(
                    $resource->getAttributeTypeTable('int'),
                    $positionRows,
                    array('value')
                );
            }
        }
        return $this;
    }

} 