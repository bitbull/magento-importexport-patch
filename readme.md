Bitbull ImportExportPatch Extension
=====================
Module to patch Mage_ImportExport module
Facts
-----
- version: 1.0.0
- extension key: Bitbull_ImportExportPatch
- [extension on Bitbucket](https://bitbucket.org/bitbull/magento-avs-fastsimpleimport-patch)

Description
-----------
This module provvide to:

- Not remove of all kind of links (related, upsell, crossel) if their are not in the csv

Requirements
------------
- PHP >= 5.2.0
- Mage_ImportExport


Compatibility
-------------
- Magento >= 1.9

Developer
---------
Bitbull Team
[http://www.bitbull.it](http://www.bitbull.it)

Licence
-------
[OSL - Open Software Licence 3.0](http://opensource.org/licenses/osl-3.0.php)

Copyright
---------
(c) 2016 Bitbull
